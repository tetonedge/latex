\documentclass[final,t]{beamer}
\mode<presentation>{
	\usetheme{CSUFAMPoster}
	%\usetheme{Montpellier}
	}
\usepackage{times}
\usepackage{graphicx} % for pdf, bitmapped graphics files
%\usepackage{amsmath,amsthm, amssymb, latexsym}
\usepackage{multirow}
\usepackage[orientation=landscape,size=custom,width=119.4,height=88.9,scale=1]{beamerposter}

\newcommand{\buhat}{\hat{\textrm{\textbf{{u}}}}}
\newcommand{\buhattil}{\tilde{\hat{\textrm{\textbf{{u}}}}}}
\newcommand{\bvhat}{\hat{\textrm{\textbf{{v}}}}} 
\newcommand{\Xhat}{\hat{X}}
\newcommand{\Uhat}{\hat{U}}
\newcommand{\Vhat}{\hat{V}}
\newcommand{\Shat}{\hat{\Sigma}}
\newcommand{\shat}{\hat{\sigma}}

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
  \title{Pose Detection of 3-D Objects Using Images Sampled on $SO(3)$, Spherical Harmonics, and Wigner-$D$ Matrices}
	\author{Randy C. Hoover$^*$, Anthony A. Maciejewski$^*$, and Rodney G. Roberts$^+$}
	\institute{Department of Electrical and Computer Engineering\\[0.2ex] 
	$^*$Colorado State University - Fort Collins, CO 80523, USA \hspace{0.5in} $^+$Florida A \& M - Florida State University Tallahassee, FL 32310, USA}


%%Object recognition and pose detection of three-dimensional (3-D) objects from two-dimensional (2-D) images are important issues in computer vision. Eigendecomposition is one computationally efficient solution to this issue.  Unfortunately, the off-line calculation for determining the appropriate subspace dimension, as well as the principal eigenimages themselves is computationally expensive.   An efficient algorithm was developed by Chang et al. for images correlated in one-dimension.  This algorithm has recently been extended to correlation in two dimensions using the spherical harmonic transform.  The issue is then deciding on the �best� tessellation of the sphere.  In this work, we analyze the Gauss-Legendre (GL) grid, an equi-angular (EA) grid, and the HEALPix grid.
%%

  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5
  \begin{document}
  \begin{frame}{} 
  \begin{picture}(0,0)
			\put(-20,40){\includegraphics<1>[scale=7]{./Figures/CSUGold.png}}
			\put(3110,50){\includegraphics<1>[scale=2]{./Figures/FAM_FS_logo.png}}
		\end{picture}
    %\vfill
    \vspace{-2ex}
    %-----------------------------------------------------------------------
    \begin{block}{\large Abstract}
      Determining the pose of three-dimensional objects from two-dimensional images has become an important issue in industrial automation applications.  Eigendecomposition represents one computationally efficient method for dealing with this class of problems.  One major drawback of using the eigendecomposition technique is the expensive off-line computation required to calculate the optimal subspace.  This off-line computational expense may preclude the use of eigendecomposition for industrial applications where time is of the essence.  In this work, we address this issue by proposing a computationally efficient algorithm for estimating the eigendecomposition to a user specified accuracy.  In particular, we sample the rotation group $SO(3)$ in a manner that allows us to take advantage of the correlation in $SO(3)$ and transform the data from the spatial domain to the spectral domain.  We then present an algorithm to estimate the eigendecomposition in this domain, thus relieving the computational burden.  Experimental results are presented to compare the proposed algorithm to the true eigendecomposition, as well as assess the computational savings.
    \end{block}
    %-----------------------------------------------------------------------
    \begin{columns}
    	\begin{column}[onlytextwidth]{.33\linewidth}
    \begin{block}{\large Introduction}
    \begin{itemize}
			\item Object recognition and pose detection of three-dimensional (3-D) objects from two-dimensional (2-D) images
			\item Eigendecomposition
			\begin{itemize}
				\item Advantage - Computationally efficient on-line
				\item Disadvantage - Expensive off-line
			\end{itemize}
			\item Overcome disadvantages by sampling $SO(3)$ appropriately
			\item Condense information due to the correlation in $SO(3)$ using spherical harmonics and Wigner-$D$ matrices
		\end{itemize}
    \end{block}
    \begin{block}{\large Preliminaries}
    \begin{itemize}
  	\item This work considers gray-scale images $\mathcal{X} \in [0, 1]^{h \times v}$
  	\begin{itemize}
			\item The images are then row-scanned to create the image vector $\mbox{\boldmath{$f$}} = \textrm{vec}(\mathcal{X}^T )$
			\item Sets of correlated image vectors are then concatenated column-wise to form $X = [\mbox{\boldmath{$f$}}_1, \cdots, \mbox{\boldmath{$f$}}_n]$
			\item subtract the mean image to get $\Xhat$ (unbiased image data matrix)
			\begin{itemize}
				\item $n=ab$, where $a$ is the number of samples defined on the sphere
				\item $b$ is the number of planar rotations captured at each sample
			\end{itemize}
		\end{itemize}
  	\item Sampling on $SO(3)$ $\mbox{\boldmath{$f$}} = \mbox{\boldmath{$f$}}(\mbox{\boldmath{$\xi$}}_{p,r})$
  	\begin{itemize}
			\item $\mbox{\boldmath{$\xi$}}_{p,r}$: $p \in \{0, \dots, a-1\}$ is the unit vector pointing at the angle of co-latitude $\beta_p \in (0,\pi)$ and the angle of longitude $\alpha_p \in [0,2 \pi)$
			\begin{itemize}
				\item parameterization of the sphere in spherical coordinates
			\end{itemize}
			\item $r \in \{0, \dots, b-1\}$ is the $r^{\text{th}}$ planar rotation $\gamma_r \in [0,2 \pi)$ at sample $p$
		\end{itemize}
		\item $SO(3)$ sampling pattern
		\begin{itemize}
			\item Hierarchical Equal Area isoLatitude Pixelization (HEALPix)
			\begin{itemize}
				\item Isolatitudinal
				\item Equal area weighting over $S^2$ 
			\end{itemize} 
		\end{itemize}
  \end{itemize}
  \begin{itemize}
     	\item SVD: $\Xhat=\Uhat \Shat \Vhat^T$
			\begin{itemize}
				\item $\Uhat$-left singular vectors (eigenimages)
				\begin{itemize}
					\item typically estimated $\tilde{\Uhat}_k$
					\item form a $k$-dimensional basis
				\end{itemize}
			\end{itemize}
		\end{itemize}	
  %\vspace{0.2in}
      \begin{figure}
				\centering
				\includegraphics[width=0.57\linewidth]{Figures/SamplesSO3.png}
			\end{figure}
			\centering
			\vspace{-0.3in}
			\textbf{\large HEALPix Sampling Pattern}
    \end{block}   
    \begin{block}{\large Quality of Estimation}
    \begin{itemize}
			\item Energy recovery ratio, (how much energy a set of basis vectors can recover)
			\item[] $\rho(\Xhat,\tilde{\Uhat}_k) = \frac{\displaystyle{\sum_{i=1}^k} ||\buhattil_i^T \Xhat ||^2}{||\Xhat||_F^2}$
			\item Change in energy, (how much additional energy is recovered)
			\item[] $\Delta \rho(\Xhat,\tilde{\Uhat}_k) = \rho(\Xhat,\tilde{\Uhat}_k)-\rho(\Xhat,\tilde{\Uhat}_{k-1})$ 
		\end{itemize}
    
    \end{block}
    \end{column}
    %-----------------------------------------------------------------------------------------------
    \begin{column}{.33\linewidth}
    \begin{block}{\large{Spherical Harmonics}}
    \begin{itemize}
			\item Spherical harmonics
			\item[] $Y_l^m(\mbox{\boldmath{$\xi$}}_{p}) = (-1)^m \sqrt{\frac{2l+1}{4 \pi} \frac{(l-m)!}{(l+m)!}} P_l^m(\text{cos}(\beta_p))e^{i m \alpha_p}$ 
		\end{itemize}
     	\begin{figure}[h]
			\centering
			\includegraphics[width=\columnwidth]{Figures/SHPlots600.png}
		\end{figure}
    \end{block}
    
    \begin{block}{\large{Spherical Harmonic Transform and Wigner-$D$ Matrices}}
    \begin{itemize}
			\item Spherical harmonic transform
			\item[] $f(\mbox{\boldmath{$\xi$}}_{p}) = \displaystyle{\sum_{l=0}^{l_{\text{max}}} \sum_{|m| \leq l}} f_l^m Y_{l}^{m}(\mbox{\boldmath{$\xi$}}_{p})$
			\item Rotation of a spherical function
			\item[] $\Lambda(\alpha,\beta,\gamma) Y_l^m(\alpha,\beta) = Y_l^m(\alpha',\beta') = \sum_{|m| \leq l} Y_l^m(\alpha,\beta) D_{m m'}^l(\alpha,\beta,\gamma)$
			\item $SO(3)$ harmonic transform
			\item[] $f(\mbox{\boldmath{$\xi$}}_{p,r}) \approx \displaystyle{\sum_{l=0}^{l_\mathrm{max}} \sum_{|m| \leq l} \sum_{|m'|\leq l}} f_{mm'}^l D_{mm'}^l(\mbox{\boldmath{$\xi$}}_{p,r})$
			\item Expansion coefficients (harmonic images)
			\item[] $f_{mm'}^l = \frac{4 \pi}{a} \displaystyle{\sum_{p=0}^{a-1} \sum_{r=0}^{b-1}} f(\mbox{\boldmath{$\xi$}}_{p,r}) D_{mm'}^{l*}(\mbox{\boldmath{$\xi$}}_{p,r})$
			\item SVD of low frequency harmonic images
			\begin{itemize}
				\item Good estimates of true eigenimages
				\item Significant computational savings 
			\end{itemize}
		\end{itemize}
    
     	
    \end{block}
    
    %---------------------------------------------------------------------------
    \begin{block}{\large Eigendecomposition Algorithm}
    	\begin{itemize}
				\item The goal is to estimate the first $k$ principal eigenimages $\tilde{\Uhat}_k$ of $\Xhat$ such that $\Delta \rho(\Xhat,\tilde{\Uhat}_k) \leq \epsilon$, where $\epsilon$ is the user specified change in energy
				\item Sample objects according to the HEALPix tessellation to construct the matrix $\Xhat$
				\begin{itemize}
					\item HEALPix uses parameter $N_{\text{side}}$ to determine sampling resolution
				\end{itemize}
				\item Compute the matrix $F$ whose $i^{\text{th}}$ row is the $SO(3)$ FFT of the $i^{\text{th}}$ row of $X$, denoted as SOFT($\Xhat$)
				\item Due to the $SO(3)$ correlation, the principal eigenimages $\tilde{\Uhat}_k$ of SVD($F$) serve as excellent estimates to those of $\Xhat$
			\end{itemize}
			\vspace{2ex}
			\textbf{\textsc{Eigendecomposition Algorithm}}
			\begin{quote}
			\begin{enumerate}
				\item  Form the matrix $F$ which is the $SOFT(\Xhat)$
				\item  Form the matrix $H$ whose columns are the ordered columns of $F$ in descending order according to their norm
				\item  Set $q = N_{\text{side}}(36 N_{\text{side}}-1) [1-(1/2)^{N+1}]$, with $N$=0 initially
				\item  Construct the matrix $H_q$ which is the matrix consisting of the first $q$ columns of $H$
				\item  Compute SVD($H_q$) = $\tilde{\Uhat}_q \tilde{\Shat}_q \tilde{\Vhat}_q^T$
				\item  If $\Delta \rho(\Xhat,\tilde{\Uhat}_q) > \epsilon$.  Let $N = N+1$ and repeat Steps 3 through 6
				\item  Return $\tilde{\Uhat}_k$ such that $\Delta \rho(\Xhat,\tilde{\Uhat}_k) \leq \epsilon$.  Note that $k \leq q$
			\end{enumerate}
			\end{quote}
    \end{block} 
    %---------------------------------------------------------------------------------
    \end{column}
    %------------------------------------------------------------------
    \begin{column}{.33\linewidth}
    \begin{block}{\large Analysis of the Results}
      \begin{figure}
				\centering
				\includegraphics[width=0.76\linewidth]{./Figures/Objects600.png}
				%\caption{\textbf{\large Ray-traced CAD models used in the analysis}}
			\end{figure}
			\centering
			\textbf{\large Ray-traced CAD models used in the analysis}\\
			%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
			\vspace{0.93in}
			\textbf{\large Analysis results for $\Delta \rho(\Xhat,\tilde{\Uhat}_k) \leq \epsilon$ with $\epsilon = 0.01$}
			\begin{table}[h!]\nonumber
			%\caption{\textbf{\large Analysis results for $\Delta \rho(\Xhat,\tilde{\Uhat}_k) \leq \epsilon$ with $\epsilon = 0.01$.}}
				\centering		
				\begin{tabular}{c|c|c|c|c|c|c|c}
				\hline
				\multirow{2}{*}{Object} & \multirow{2}{*}{$k$} & \multirow{2}{*}{$\bar{k}_{\text{min}}$}  & \multicolumn{2}{|c}{Time [hours]} & \multicolumn{3}{|c}{Energy $\rho$ [\%]}\\
				\cline{4-8}
		   		&   &  & True & Proposed & True & Proposed& At $\bar{k}_{\text{min}}$\\
				\hline
				\input{./Files/ITIP.txt}
		 		\hline
				\end{tabular}
 			\end{table}
			%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
			
			
			
			%%\begin{figure}
%%				\centering
%%				\includegraphics[width=\linewidth]{./Figures/PoseTest600.png}
%%			\end{figure}
%%			\centering
%%			\textbf{\large Subspace dimension for 72 test poses}
    \end{block}
    \begin{block}{\large Discussion}
    \begin{itemize}
			\item Quality of the estimates of $\tilde{\Uhat}_k$ are nearly equivalent to the true left singular vectors 
			\item Proposed algorithm is approximately 40 times faster than computing the SVD($\Xhat$)
			\item Transform is lossy, however the loss is minimal with respect to the computational savings
			\item Test poses show accurate estimation is possible at a significant computational savings
		\end{itemize}
    \end{block}
    \end{column}
    \end{columns}
  \end{frame}
\end{document}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Local Variables: 
%%% mode: latex
%%% TeX-PDF-mode: t


